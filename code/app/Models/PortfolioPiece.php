<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PortfolioPiece
 * @package App\Models
 * @version November 9, 2017, 1:04 am UTC
 *
 * @property string name
 * @property string description
 * @property string url
 * @property string image
 */
class PortfolioPiece extends Model
{

    public $fillable = [
        'name',
        'description',
        'url',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'url' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    function skills() {
        return $this->belongsToMany('App\Models\Skill', 'portfolio_pieces_skills',
            'portfolio_piece_id', 'skill_id');
    }

}
