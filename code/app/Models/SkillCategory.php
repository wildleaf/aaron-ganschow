<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SkillCategory
 * @package App\Models
 * @version November 9, 2017, 1:01 am UTC
 *
 * @property string name
 * @property boolean order
 */
class SkillCategory extends Model
{

    public $fillable = [
        'name',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    function skills()
    {
        return $this->hasMany('App\Models\Skill');
    }
    
}
