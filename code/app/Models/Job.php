<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Job
 * @package App\Models
 * @version November 9, 2017, 12:38 am UTC
 *
 * @property string title
 * @property string description
 * @property date start_date
 * @property date end_date
 */
class Job extends Model
{

    public $fillable = [
        'title',
        'description',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_name' => 'string',
        'company_location' => 'string',
        'company_url' => 'string',
        'company_logo' => 'string',
        'title' => 'string',
        'description' => 'string',
        'start_date' => 'date',
        'end_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    function skills() {
        return $this->belongsToMany('App\Models\Skill', 'job_skills',
            'job_id', 'skill_id');
    }


}
