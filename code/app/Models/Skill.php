<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Skill
 * @package App\Models
 * @version November 9, 2017, 1:00 am UTC
 *
 * @property string name
 * @property string description
 * @property integer percentage
 * @property date start_date
 * @property integer order
 */
class Skill extends Model
{


    public $fillable = [
        'skill_category_id',
        'name',
        'skill_logo',
        'description',
        'percentage',
        'start_date',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'skill_logo' => 'string',
        'description' => 'string',
        'percentage' => 'integer',
        'start_date' => 'date',
        'order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    function skillcategory()
    {
        return $this->belongsTo('App\Models\SkillCategory', 'skill_category_id', 'id', 'skill_categoryies');
    }

    function jobs()
    {
        return $this->belongsToMany('App\Models\Job', 'job_skills', 'skill_id', 'job_id');
    }

    function portfolios()
    {
        return $this->belongsToMany('App\Models\PortfolioPiece', 'portfolio_pieces_skills', 'skill_id', 'portfolio_piece_id');
    }


}
