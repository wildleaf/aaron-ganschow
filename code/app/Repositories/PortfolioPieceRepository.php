<?php

namespace App\Repositories;

use App\Models\PortfolioPiece;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PortfolioPieceRepository
 * @package App\Repositories
 * @version November 9, 2017, 1:04 am UTC
 *
 * @method PortfolioPiece findWithoutFail($id, $columns = ['*'])
 * @method PortfolioPiece find($id, $columns = ['*'])
 * @method PortfolioPiece first($columns = ['*'])
*/
class PortfolioPieceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'url',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PortfolioPiece::class;
    }
}
