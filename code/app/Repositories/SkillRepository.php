<?php

namespace App\Repositories;

use App\Models\Skill;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SkillRepository
 * @package App\Repositories
 * @version November 9, 2017, 1:00 am UTC
 *
 * @method Skill findWithoutFail($id, $columns = ['*'])
 * @method Skill find($id, $columns = ['*'])
 * @method Skill first($columns = ['*'])
*/
class SkillRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'percentage',
        'start_date',
        'order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Skill::class;
    }
}
