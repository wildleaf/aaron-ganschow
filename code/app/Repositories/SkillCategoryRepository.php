<?php

namespace App\Repositories;

use App\Models\SkillCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SkillCategoryRepository
 * @package App\Repositories
 * @version November 9, 2017, 1:01 am UTC
 *
 * @method SkillCategory findWithoutFail($id, $columns = ['*'])
 * @method SkillCategory find($id, $columns = ['*'])
 * @method SkillCategory first($columns = ['*'])
*/
class SkillCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SkillCategory::class;
    }
}
