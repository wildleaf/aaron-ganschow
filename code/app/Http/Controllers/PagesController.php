<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Skill;
use App\Models\Job;
use App\Models\PortfolioPiece;

class PagesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index() {
        return view('pages.index');
    }
    public function skills() {

        $skills = Skill::orderBy('skill_category_id','asc')->orderBy('order','asc')->get();
        return view('pages.skills',['skills'=>$skills]);
    }
    public function experience() {
        $jobs = Job::orderBy('start_date','desc')->get();
        return view('pages.experience', ['jobs'=>$jobs]);
    }
    public function portfolio() {
        $pieces = PortfolioPiece::orderBy('created_at','asc')->get();
        return view('pages.portfolio', ['pieces'=>$pieces]);
    }
    public function about() {
        return view('pages.about');
    }
    public function thanks() {
        return view('pages.thanks');
    }
}
