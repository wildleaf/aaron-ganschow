<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_location'); // Replace string with location/gmaps data
            $table->string('company_url');
            $table->string('company_logo');
            $table->string('title');
            $table->longText('description'); // Assuming HTML for this for now
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });

        Schema::create('job_skills', function (Blueprint $table) {
           $table->integer('job_id');
           $table->integer('skill_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('job_skills');
    }
}
