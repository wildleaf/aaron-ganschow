<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioPiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_pieces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description');
            $table->string('url');
            $table->string('image');
            $table->timestamps();
        });

        Schema::create('portfolio_pieces_skills', function(Blueprint $table) {
           $table->integer('portfolio_piece_id');
           $table->integer('skill_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_pieces');
        Schema::dropIfExists('portfolio_pieces_skills');
    }
}
