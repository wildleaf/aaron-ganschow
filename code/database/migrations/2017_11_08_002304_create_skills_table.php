<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('skill_category_id'); // foriegn key for category
            $table->string('skill_logo');
            $table->string('name');
            $table->longText('description');
            $table->tinyInteger('percentage');
            $table->date('start_date');
            $table->tinyInteger('order', false, true);
            $table->timestamps();
        });

        Schema::create('related_skills', function(Blueprint $table) {
           $table->integer('skill_1_id');
           $table->integer('skill_2_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills');
        Schema::dropIfExists('related_skills');
    }
}
