<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aaron Ganschow | {!! trans('aganschow.sections.' . $section . '.title') !!}</title>
    <link href="/css/app.css" type="text/css" rel="stylesheet" />

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
    <meta name="application-name" content="Aaron Ganschow | Lead PHP Developer"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/mstile-144x144.png" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-891180-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-891180-3');
    </script>

</head>
<body>
<header class="container">
    <div class="row">
        <div class="col-sm-12">
            <nav id="navbar-primary" class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-toggle" href="/" style="float:left;"><img src="/img/logo.png" width="100" alt="Aaron Ganschow"></a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse" style="background: #FFF; margin-top: 18px;">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar" style="background: #000; color: #FFF;"></span>
                            <span class="icon-bar" style="background: #000; color: #FFF;"></span>
                            <span class="icon-bar" style="background: #000; color: #FFF;"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-primary-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/skills">Skills</a></li>
                            <li><a href="/experience">Experience</a></li>
                            <li class="logo hidden-xs"><a href="/"><img src="/img/logo.png" width="200" alt="Aaron Ganschow"><h1 class="text-hide sr-only sr-only-focusable">Aaron Ganschow</h1></a></li>
                            <li><a href="/portfolio">Portfolio</a></li>
                            <li><a href="/about">About</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            @include('aganschow.header.video')
            <h2>{!! trans('aganschow.sections.' . $section . '.title') !!}</h2>
        </div>
    </div>
</header>

<section class="container" id="body-content-{{$section}}">
    <div class="row">
        <div class="col-xs-12">
            @yield('content')
        </div>
    </div>
</section>

<br />

@include('aganschow.footer')

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>
</html>
