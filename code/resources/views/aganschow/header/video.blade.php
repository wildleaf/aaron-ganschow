<div class="embed-responsive embed-responsive-16by9">
        <video class="embed-responsive-item" autoplay muted poster="/video/{{$section}}.jpg">
            <source src="video/{{$section}}.mp4" type="video/mp4">
            <source src="video/{{$section}}.webm" type="video/webm">
            <source src="video/{{$section}}.ogv" type="video/ogv">
        </video>
</div>
<div class="video-info">
    <i class="glyphicon glyphicon-question-sign" data-toggle="collapse" href="#section-video-details"></i>
    <div class="collapse video-details" id="section-video-details">
        {!! trans('aganschow.sections.' . $section . '.video_footnote') !!}
    </div>
</div>
