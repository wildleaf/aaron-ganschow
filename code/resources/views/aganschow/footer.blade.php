
<h2 class="text-center">Contact Me</h2>
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <div class="card border-0">
                <div class="card-body text-center">
                    <a href="/Aaron-Ganschow-Resume.docx" onclick="var that=this;gtag('event','download', {'event_category':'Documents','event_label':this.href});setTimeout(function(){location.href=that.href;},200);return false;">
                    <i class="glyphicon glyphicon-save-file" aria-hidden="true" style="font-size: 40pt"></i><br />
                    Download Resume
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="card border-0">
                <div class="card-body text-center">
                    <i class="glyphicon glyphicon-phone-alt" aria-hidden="true" style="font-size: 40pt;"></i>
                    <p>(602) 503-0332</p>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="card border-0">
                <div class="card-body text-center">
                    <i class="glyphicon glyphicon-map-marker" aria-hidden="true" style="font-size: 40pt"></i>
                    <address>Greater Phoenix, AZ</address>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="card border-0">
                <div class="card-body text-center">
                    <a href="mailto:aaron@ganschow.us" onclick="var that=this;gtag('event','initiated',{'event_category':'Emails','event_label':'Footer Email'});setTimeout(function(){location.href=that.href;},200);return false;">
                    <i class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size: 40pt"></i><br />
                    <p>aaron@ganschow.us</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-10 col-md-push-1" >
            <div class="well">
                <form action="https://service.capsulecrm.com/service/newlead" method="post" id="crmform"  onsubmit="var that=this;gtag('event','submitted',{'event_category':'CRM Forms','event_label':'contact form'});">
                    <input type="hidden" name="FORM_ID" value="748be951-6b83-4fd9-a914-cc8f3f970f6e">
                    <input type="hidden" name="COMPLETE_URL" value="http://aaron.ganschow.us/thanks">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Name</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                                </span>
                                    <input type="text" class="form-control" id="name" name="PERSON_NAME" placeholder="Awesome Visitor" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email Address</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                    <input type="email" class="form-control" id="email" name="EMAIL" placeholder="visitor@email.adr" required="required" /></div>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Phone Number</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span>
                                </span>
                                    <input type="text" class="form-control" id="email" name="Phone" placeholder="(000) 000-0000" /></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subject">
                                    Subject</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-edit"></span>
                                </span>
                                    <select id="subject" name="CUSTOMFIELD[Subject]" class="form-control" required="required">
                                        <option value="" selected="">Choose One:</option>
                                        <option value="service">Product Support</option>
                                        <option value="fulltime">Full Time Opportunity</option>
                                        <option value="contract">Contract Opportunity</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span>
                                </span>
                                    <textarea name="NOTE" id="message" class="form-control" rows="5" cols="25" required="required"
                                              placeholder="Carefully crafted note"></textarea>
                                </div>
                                <br />
                                <input id="comment-field" type="text" name="COMMENT" />
                                <button type="submit" class="btn btn-primary pull-right btn-large" id="btnContactUs">
                                    Send Message</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>