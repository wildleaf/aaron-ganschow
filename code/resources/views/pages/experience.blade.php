@extends('aganschow/layout')

@php ($section = 'experience')

@section('title', 'Work Experience')

@section('content')
    <div class="row">
    @foreach($jobs as $job)
            <div class="col-sm-10 col-sm-push-1">
                <div class="portfolio-piece">
                    <div class="caption">
                        <div class="logo">
                            <a href="{{$job->company_url}}" target="_blank">
                                <img class="img-responsive" src="/storage/{{$job->company_logo}}" alt="{{$job->company_name}}">
                            </a>
                        </div>
                        <h3>{{$job->title}}</h3>
                        <h4>
                            {{$job->company_name}} // {{$job->company_location}}
                            <br>
                            {{date('F Y', strtotime($job->start_date))}} &ndash; {{date('F Y', strtotime($job->end_date))}}

                        </h4>
                        <p class="tech">
                            @foreach($job->skills as $skill)
                                <span class="badge">{{$skill->name}}</span>
                            @endforeach
                        </p>
                        {!! $job->description !!}
                    </div>
                </div>
            </div>

    @endforeach
    </div>

@endsection