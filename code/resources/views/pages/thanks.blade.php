@extends('aganschow/layout')

@php ($section = 'contact')

@section('title', 'Contact Submission')

@section('content')

    <div class="lead col-sm-10 col-sm-push-1">
        <h2>Your request for contact has been received</h2>
        <p>
            Thank you for your submission.  You've been automatically added to <a href="http://capsulecrm.com">CapsuleCRM</a> for response. Tidy and free service for small busniesses and individuals.
        </p>
        <h3>Yes, you've essentially been added to my custom sales queue.</h3>
    </div>

@endsection