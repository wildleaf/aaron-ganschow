@extends('aganschow/layout')

@php ($section = 'portfolio')

@section('title', 'Digital Portfolio')

@section('content')
    <div class="row">
        @foreach($pieces as $piece)
            <div class="col-sm-10 col-sm-push-1">
                <div class="portfolio-item">
                    <div class="preview">
                       <img src="/storage/{{$piece->image}}" alt="{{$piece->name}}" class="img-responsive thumbnail">
                    </div>
                    <div class="caption collapse-group">
                        <h3>{{$piece->name}}
                            @if($piece->url != "NA")
                            <a class="btn btn-xs btn-primary" href="{{$piece->url}}" target="_blank">Visit Site
                                <i class="glyphicon glyphicon-new-window"></i>
                            </a>
                            @endif
                        </h3>
                        <p class="tech">
                            @foreach($piece->skills as $skill)
                                <span class="badge">{{$skill->name}}</span>
                            @endforeach
                            </p>
                        {!! $piece->description !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection