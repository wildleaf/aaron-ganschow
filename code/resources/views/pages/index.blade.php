@extends('aganschow/layout')

@php ($section = 'index')

@section('title', 'Lead PHP Developer')

@section('content')

    <div class="lead col-sm-10 col-sm-push-1">
        <h2>Located in Phoenix, Arizona, I am a full stack PHP web developer with a penchant for problem solving and an eye for UI.</h2>
        <p>I have spent nearly half my life attempting to perfect the web development paradigm. It will never be complete and I will always want to learn more. I love organized, structured and clean programs with a focus on usability, stability, and security. I get satisfaction out of the puzzles in the task.</p>
        <p>I have vast experience in custom Drupal theme and module development as well as many enterprise implementations of frontend frameworks, backend frameworks, and data structures. In the end, my true passion is diving into a project, learning everything I can about a business, and exceeding their expectations.</p>
        <a href="/about" class="btn btn-lg btn-danger pull-right">Learn More</a>
    </div>

    <div class="col-xs-12" style="background: #DDF6FF; text-align:center;">
        <h2>Technical Skills</h2>
        <blockquote class="quote-card hidden-xs">
            <p>what I do have are a very particular set of skills; skills I have acquired over a very long career</p>
            <footer class="cite">Liam Neeson as Bryan Mills from <i><a href="http://www.imdb.com/title/tt0936501/">Taken</a></i></footer>
        </blockquote>
        <img src="/img/skills/php.png" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/mysql.png" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/html-css.png" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/sass.png" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/git.png" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/jquery.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/bootstrap.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/drupal.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/wordpress.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/bigcommerce.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/magento.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/laravel.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <img src="/img/skills/symfony.png" class="hidden-xs" width="120" style="padding:10px; margin: 5px;" />
        <div style="width: 120px; display: inline-block; padding: 0; margin: 5px;">
            <a href="/skills" class="btn btn-block btn-lg btn-warning">See All</a>
        </div>
        <br />
        <br />
        <br />
    </div>
    <div class="col-xs-12" style=" text-align:center;">
        <h2>Experience</h2>
        <blockquote class="quote-card hidden-xs">
            <p>Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do.</p>
            <footer class="cite">Steve Jobs</footer>
        </blockquote>
        <a class="btn btn-lg btn-info" role="button" href="/experience/">View Work Experience</a>&nbsp;<a class="btn btn-lg btn-primary" href="/Aaron-Ganschow-Resume.docx" onclick="var that=this;gtag('event','download', {'event_category':'Documents','event_label':this.href});setTimeout(function(){location.href=that.href;},200);return false;" role="button">Download Resume <span class="glyphicon glyphicon-download-alt"></span></a>
        <br />
        <br />
        <br />

    </div>
    <div class="col-xs-12" style="background: #666; color: #ECECEC; text-align:center;">
        <h2>Portfolio</h2>
        <blockquote class="quote-card hidden-xs">
            <p>Technology, like art, is a soaring exercise of the human imagination.</p>
            <footer class="cite"><a href="https://en.wikipedia.org/wiki/Daniel_Bell">Daniel Bell</a></footer>
        </blockquote>
        <div class="col-xs-6 col-md-3">
            <a href="/portfolio?click=fordurban"><img src="/storage/portfolio-pieces/November2017/3vTf4hHvxskXxkSQtSwI.png" alt="Ford Urban" class="img-responsive thumbnail"></a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="/portfolio?click=ingestiondigest"><img src="/storage/portfolio-pieces/November2017/R1gbb8Uj8orpIlHQwygg.png" alt="Ingestion Digest" class="img-responsive thumbnail"></a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="/portfolio?click=asuherberger"><img src="/storage/portfolio-pieces/November2017/Z2wvV0q6BrjGc7FVgS3v.jpg" alt="ASU Herberger Academy" class="img-responsive thumbnail"></a>
        </div>
        <div class="col-xs-6 col-md-3">
            <a href="/portfolio?click=uofa"><img src="/storage/portfolio-pieces/November2017/NilPBSPZfYkVbyCVvaL7.png" alt="UofA Libraries" class="img-responsive thumbnail"></a>
        </div>
        <div class="col-xs-12">
            <a href="/portfolio?click=view" class="btn btn-lg btn-success">
                    View Portfolio
            </a>
            <br />
            <br />
            <br />
        </div>
    </div>
@endsection