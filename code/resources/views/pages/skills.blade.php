@extends('aganschow/layout')

@php ($section = 'skills')

@section('title', 'Technical Skills')

@section('content')
    @php ($category = '')
    @foreach($skills as $skill)
        @if($skill->skillcategory->name != $category)
            @php ($category = $skill->skillcategory->name)
            <br />
            <br />
            <h3>{{ $skill->skillcategory->name }}</h3>
    @endif
        <article style="display: inline-block; width: 150px; text-align: center;">
            <img src="/storage/{{ $skill->skill_logo }}"  width="120" style="padding:10px; margin: 5px;" alt="{{ $skill->name }} " />
            <h4>{{ $skill->name }}</h4>
            <!--p>Jobs: {{$skill->jobs->count()}}</p>
            <p>Portfolio: {{$skill->portfolios->count()}}</p-->

            <div class="xp-bar">
                <div class="xp-bar-progress" role="progressbar" aria-valuenow="{{$skill->percentage}}" aria-valuemin="0" aria-valuemax="{{$skill->percentage}}" style="width:{{$skill->percentage}}%;">
                    {{date('Y', strtotime($skill->start_date))}}
                </div>
            </div>
        </article>
    @endforeach
@endsection