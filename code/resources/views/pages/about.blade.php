@extends('aganschow/layout')

@php ($section = 'about')

@section('title', 'About Me')

@section('content')

    <div class="col-sm-10 col-sm-push-1">
        <h2>Located in Phoenix, Arizona, I am a full stack PHP web developer with a penchant for problem solving and an eye for UI.</h2>
        <p>I have spent nearly half my life attempting to perfect the web development paradigm. It will never be complete and I will always want to learn more. I love organized, structured and clean programs with a focus on usability, stability, and security. I get satisfaction out of the puzzles in the task.</p>
        <p>I have vast experience in custom Drupal theme and module development as well as many enterprise implementations of frontend frameworks, backend frameworks, and data structures. In the end, my true passion is diving into a project, learning everything I can about a business, and exceeding their expectations.</p>

        <h2>Look a little deeper</h2>
        <p>
            I started programming in 1996. QBASIC was my way of life, until the next year when I learned Pascal, then C++ and Visual Basic. In 1998 I developed the first electronic <a href="/img/aaron-ganschow-washington-school-district.pdf">Math Aptitude Test for the Washington School District</a>. Soon thereafter I recieved my first contract position to develop an intranet for MedTronic's Facilities department using ColdFusion. In quick succession I received a perl project for a local vitamin company through a referral from my auto teacher.
        </p>
        <p>
            I learned most about developing in perl, HTML, and PHP by giving myself small projects, hosting websites for friends, and pushing for any contract that would allow me a platform to learn.  Early on I found my strongest skill was learning new development paradigms. I still seek out what is new and most efficient in my preferred platforms. I am a Senior PHP Developer and like it that way. I love working on front end and back end equally and often have a difficult time answering which I prefer. Ultimately, you need both to achieve the ideal pipeline of information to the user.
        </p>
        <h2>After hours (ooooh la laaaa)</h2>
        <p>
            When I am not avidly typing away at PHPStorm, I can be found experimenting in the kitchen, or finding new eclectic dining in the valley.  I am a bit of a foodie. And loquacious. I have my degree in English so I sometimes skirt the strict rules of language. Effective language has power.
        </p>
        <p>
            I thank <a href="/img/aaron-sewing.pdf">Saguaro Ranch Park and Carole DeCosmo</a> for my obsession with crafts, sewing, and specifically cosplay since age 11. Halloween is my Christmas. Comicon should be a locally mandated holiday.  I joke, but I am also taking a few days off to experience it!
        </p>
    </div>

@endsection