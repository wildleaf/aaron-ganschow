<?php

return [

    'sections'              => [
        'index' => [
            'title' => "Lead PHP Developer",
            'video_footnote' => "This beautiful video was found on <a href='https://www.youtube.com/watch?v=5yRL4fpLTMs' target='_blank'>YouTube</a>. A Saguaro cactus in the Catalina Mountains, Tucson, Arizona experiencing a sunrise. Rights were given by <a href='https://www.youtube.com/channel/UCXeI9F5bKVsxCsdzSN-LprQ'>Greg McCown</a> to use a section for this site.</a>",
        ],
        'skills' => [
            'title' => "Technical Skills",
            'video_footnote' => "This video was downloaded from <a href='https://www.videezy.com/music-related/2654-mixing-on-sound-board-hd-stock-footage'>Videezy</a> with a Creative Commons Attribution License. Development skills in video seems to come down to writing code on a screen. This is prettier and provides a visual corollary to technical skills. ",
        ],
        'experience' => [
            'title' => "Work Experience",
            'video_footnote' => "Provided by <a href='https://coverr.co/'>Coverr</a>, this video reminds me of working at the <a href='https://www.glg.com/'>Garrigan Lyman Group</a>. Really, one of my favorite video choices for this site.",
        ],
        'portfolio' => [
            'title' => "Digital Portfolio",
            'video_footnote' => "Footage clipped from <a href='https://www.youtube.com/watch?v=gmDQcY5mzHg' target='_blank'>Jacques Herzog: The Pérez Art Museum \"is a naked structure\"</a> with Creative Commons Attribution.",
        ],
        'about' => [
            'title' => "About Me",
            'video_footnote' => "Footage clipped from <a href='https://www.youtube.com/watch?v=LeCVuuJhkQY' target='_blank'>Stock Footages : People Eating Out</a> with Creative Commons Zero (CC0) license.",
        ],
        'contact' => [
            'title' => "Contact Submission",
            'video_footnote' => "Clip provided by <a href='https://coverr.co/'>Coverr</a>.  I personally use an Android, but their 'Android Scroll' video has a Playboy monthly newsletter in the list of e-mails. So, I used the iPhone one.",
        ],
    ],

];
