-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),(4,1,'title','text','Title',1,1,1,1,1,1,'',4),(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(8,1,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),(14,2,'id','number','id',1,0,0,0,0,0,'',1),(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),(16,2,'title','text','title',1,1,1,1,1,1,'',3),(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),(25,2,'image','image','image',0,1,1,1,1,1,'',12),(26,3,'id','number','id',1,0,0,0,0,0,'',1),(27,3,'name','text','name',1,1,1,1,1,1,'',2),(28,3,'email','text','email',1,1,1,1,1,1,'',3),(29,3,'password','password','password',0,0,0,1,1,0,'',4),(30,3,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(31,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),(32,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),(33,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(34,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),(35,5,'id','number','id',1,0,0,0,0,0,'',1),(36,5,'name','text','name',1,1,1,1,1,1,'',2),(37,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(38,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(39,4,'id','number','id',1,0,0,0,0,0,'',1),(40,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(41,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),(42,4,'name','text','name',1,1,1,1,1,1,'',4),(43,4,'slug','text','slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(44,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),(45,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(46,6,'id','number','id',1,0,0,0,0,0,'',1),(47,6,'name','text','Name',1,1,1,1,1,1,'',2),(48,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(49,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(50,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(51,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),(52,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),(53,3,'role_id','text','role_id',1,1,1,1,1,1,'',9),(54,7,'id','hidden','Id',1,0,0,0,0,0,NULL,1),(55,7,'name','text','Name',1,1,1,1,1,1,NULL,2),(56,7,'order','number','Order',1,1,1,1,1,1,NULL,3),(57,7,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),(58,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),(59,8,'id','hidden','Id',1,0,0,0,0,0,NULL,1),(60,8,'skill_category_id','hidden','Skill Category Id',1,0,0,0,0,0,NULL,2),(61,8,'skill_logo','image','Skill Logo',1,1,1,1,1,1,NULL,4),(62,8,'name','text','Name',1,1,1,1,1,1,NULL,5),(63,8,'description','rich_text_box','Description',1,1,1,1,1,1,NULL,6),(64,8,'percentage','number','Percentage',1,1,1,1,1,1,NULL,7),(65,8,'start_date','date','Start Date',1,1,1,1,1,1,NULL,8),(66,8,'order','number','Order',1,1,1,1,1,1,NULL,9),(67,8,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,10),(68,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,11),(69,8,'skill_belongsto_skill_category_relationship','relationship','Category',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\SkillCategory\",\"table\":\"skill_categories\",\"type\":\"belongsTo\",\"column\":\"skill_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}',3),(70,9,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(71,9,'company_name','text','Company Name',1,1,1,1,1,1,NULL,2),(72,9,'company_location','text','Company Location',1,1,1,1,1,1,NULL,3),(73,9,'company_url','text','Company Url',1,1,1,1,1,1,NULL,4),(74,9,'company_logo','image','Company Logo',1,1,1,1,1,1,NULL,5),(75,9,'title','text_area','Title',1,1,1,1,1,1,NULL,6),(76,9,'description','rich_text_box','Description',1,1,1,1,1,1,NULL,7),(77,9,'start_date','date','Start Date',1,1,1,1,1,1,NULL,8),(78,9,'end_date','date','End Date',1,1,1,1,1,1,NULL,9),(79,9,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,10),(80,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,11),(81,9,'job_belongstomany_skill_relationship','relationship','skills',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Skill\",\"table\":\"skills\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"job_skills\",\"pivot\":\"1\"}',12),(83,10,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(84,10,'name','text','Name',1,1,1,1,1,1,NULL,2),(85,10,'description','rich_text_box','Description',1,1,1,1,1,1,NULL,3),(86,10,'url','text','Url',1,1,1,1,1,1,NULL,4),(87,10,'image','image','Image',1,1,1,1,1,1,NULL,5),(88,10,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,6),(89,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(90,10,'portfolio_piece_belongstomany_skill_relationship','relationship','skills',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Skill\",\"table\":\"skills\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"portfolio_pieces_skills\",\"pivot\":\"1\"}',8),(91,7,'skill_category_hasmany_skill_relationship','relationship','skills',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Skill\",\"table\":\"skills\",\"type\":\"hasMany\",\"column\":\"skill_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}',6);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,'2017-11-20 21:28:10','2017-11-20 21:28:10'),(7,'skill_categories','skill-categories','Skill Category','Skill Categories',NULL,'App\\Models\\SkillCategory',NULL,NULL,NULL,1,0,'2017-11-20 22:02:08','2017-11-20 22:02:08'),(8,'skills','skills','Skill','Skills',NULL,'App\\Models\\Skill',NULL,NULL,NULL,1,0,'2017-11-20 22:02:56','2017-11-20 22:02:56'),(9,'jobs','jobs','Job','Jobs',NULL,'App\\Models\\Job',NULL,NULL,NULL,1,0,'2017-11-20 22:05:28','2017-11-20 22:05:28'),(10,'portfolio_pieces','portfolio-pieces','Portfolio Piece','Portfolio Pieces',NULL,'App\\Models\\PortfolioPiece',NULL,NULL,NULL,1,0,'2017-11-20 22:08:06','2017-11-20 22:08:06');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_skills`
--

DROP TABLE IF EXISTS `job_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_skills` (
  `job_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_skills`
--

LOCK TABLES `job_skills` WRITE;
/*!40000 ALTER TABLE `job_skills` DISABLE KEYS */;
INSERT INTO `job_skills` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,11),(1,13),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,11),(2,14),(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,12),(3,14),(4,1),(4,2),(4,3),(4,4),(4,6),(4,7),(4,9),(4,10),(4,14),(5,1),(5,3),(5,10),(5,14),(6,1),(6,2),(6,3),(6,4),(6,10),(1,14),(1,15),(1,16),(1,20),(1,21),(1,23),(1,24),(1,26),(1,31),(1,32),(1,33),(1,34),(1,35),(2,15),(2,16),(2,20),(2,21),(2,23),(2,31),(2,33),(2,34),(2,35),(2,39),(7,1),(7,2),(7,14),(7,18),(7,28),(7,37),(8,1),(8,2),(8,3),(8,4),(8,7),(8,10),(8,14),(8,22),(8,40),(9,1),(9,2),(9,3),(9,4),(9,7),(9,10),(9,14),(9,17),(9,22),(10,1),(10,2),(10,3),(10,4),(10,5),(10,6),(10,7),(10,8),(10,9),(10,10),(10,11),(10,12),(10,14),(10,15),(10,16),(10,17),(10,19),(10,20),(10,23),(10,26),(10,27),(10,30),(10,31),(10,33),(10,34),(10,35),(10,38),(10,39),(10,40),(6,5),(6,6),(6,7),(6,11),(6,14),(6,15),(6,21),(6,26),(6,27),(6,28),(6,31),(6,33),(6,34),(6,35),(5,2),(5,4),(5,5),(5,6),(5,7),(5,15),(5,16),(5,21),(5,26),(5,31),(5,33),(5,34),(5,35),(5,39),(4,5),(4,12),(4,15),(4,16),(4,23),(4,25),(4,26),(4,27),(4,29),(4,30),(4,33),(4,34),(4,35),(4,39),(3,9),(3,10),(3,15),(3,16),(3,21),(3,23),(3,25),(3,26),(3,27),(3,31),(3,33),(3,34),(3,35),(3,39);
/*!40000 ALTER TABLE `job_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'Mt Baker Vapor','Mesa, AZ','https://www.mtbakervapor.com','jobs/November2017/eEJcAx4oqx52edjDRyE5.png','Lead Web Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Worked directly with owners and stakeholders to concept and quote all projects on web</li>\r\n<li style=\"box-sizing: border-box;\">Managed migration from blueprint to Stencil theming through BigCommerce</li>\r\n<li style=\"box-sizing: border-box;\">Produced automated reporting for manufacturing and purchasing projections</li>\r\n<li style=\"box-sizing: border-box;\">Managed integrations of over 30 third-party vendors for shop management</li>\r\n</ul>','2016-08-08','2017-11-10','2017-11-20 23:20:00','2017-11-20 23:42:03'),(2,'Ignite Travel','Broadbeach, QLD','https://www.ignitetravel.com/','jobs/November2017/zC2sUJJoAGqWAyYYALP2.png','Lead Web Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Managed custom multi-site Wordpress solution for&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;\" href=\"http://www.myholidaycentre.com.au/\">11 travel brands</a></li>\r\n<li style=\"box-sizing: border-box;\">Extended and created custom modules and plugins including an object oriented custom type framework to ease the use of custom post types.</li>\r\n<li style=\"box-sizing: border-box;\">Initiated continuous integration by creating custom bitbucket webhook suite</li>\r\n<li style=\"box-sizing: border-box;\">Designed wireframes and implementations for additional company websites tying into custom and third party APIs</li>\r\n</ul>','2015-07-01','2016-03-10','2017-11-20 23:22:00','2017-11-20 23:23:17'),(3,'Julep','Seattle, WA','https://www.julep.com/','jobs/November2017/nHxhHnS47wWa1u5Ykatu.jpg','Senior Web Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Created and extended&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;\" href=\"http://docs.seleniumhq.org/docs/05_selenium_rc.jsp#how-selenium-rc-works\">Selenium suite</a>&nbsp;for multi-browser automated testing with&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;\" href=\"https://saucelabs.com/\">Saucelabs</a></li>\r\n<li style=\"box-sizing: border-box;\">Planned and documented new projects for the Web Team</li>\r\n<li style=\"box-sizing: border-box;\">Upgraded sitewide integration of Facebook from v1 to v2.3</li>\r\n<li style=\"box-sizing: border-box;\">Built custom Magento plugins for extending marketing requirements</li>\r\n</ul>','2014-11-01','2015-03-15','2017-11-20 23:25:00','2017-11-21 05:55:52'),(4,'Garrigan Lyman Group','Seattle, WA','http://www.glg.com/','jobs/November2017/9kmBlevJnrQybLyHzIkI.png','Contracted Senior Web Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Created ground-up conversion of&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;\" href=\"https://www.glg.com/digital-agency-work/clients/toyo-tires/toyo-tires-website-redesign\">toyotires.com</a>&nbsp;site from Drupal 6 to Drupal 7</li>\r\n<li style=\"box-sizing: border-box;\">Acted as primary technical contact for any Drupal needs</li>\r\n<li style=\"box-sizing: border-box;\">Envisioned and implemented a database controlled ruleset and dynamic variables for matching tires to a vehicle database that could be manipulated in the future</li>\r\n<li style=\"box-sizing: border-box;\">Designed a universal&nbsp;<a style=\"box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration-line: none;\" href=\"https://www.vagrantup.com/\">Vagrant box</a>&nbsp;to host local PHP development for all developers</li>\r\n<li style=\"box-sizing: border-box;\">Estimated timelines and suggested alternative time saving development solutions</li>\r\n</ul>','2014-04-15','2014-10-15','2017-11-20 23:27:00','2017-11-21 05:54:05'),(5,'smashing ideas','Seattle, WA','http://www.smashingideas.com/','jobs/November2017/UTQCcGXRQZKwFjDfXOFV.png','Senior Technical Lead','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Managed team of developers and QA and acted as technical reference for the team</li>\r\n<li style=\"box-sizing: border-box;\">Communicated technical issues with Sony Pictures Television staff members in Los Angeles</li>\r\n<li style=\"box-sizing: border-box;\">Revised new feature process for documentation and tracking utilizing Functional Specs, Technical Requirements, and refined JIRA workflow</li>\r\n<li style=\"box-sizing: border-box;\">Developed new features, repaired bugs and determined next steps and estimates for a platform with hundreds of media rich sites from a single Drupal 7 codebase</li>\r\n</ul>','2013-06-15','2014-03-01','2017-11-20 23:29:00','2017-11-21 05:52:46'),(6,'Thunderbird.edu','Glendale, AZ','http://www.thunderbird.edu/','jobs/November2017/nbVwfBdPQ43AHdDwtazY.png','Senior Web Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Wrote detailed content management documentation for multiple stakeholder groups</li>\r\n<li style=\"box-sizing: border-box;\">Trained beginning to advanced users on managing a highly permissioned multi-site Drupal 7 instance with workflow for all content</li>\r\n<li style=\"box-sizing: border-box;\">Defined project requirements for development of modules and site functionality</li>\r\n<li style=\"box-sizing: border-box;\">Created project plans for additional features and sites in conjunction with multiple departments</li>\r\n</ul>','2012-08-10','2013-06-15','2017-11-20 23:31:00','2017-11-21 05:51:22'),(7,'Kineo Inc','Chandler, AZ','http://www.kineo.com/us','jobs/November2017/YxY5XHNpvb01c6MYPeEx.png','Senior Technical Consultant','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Released the largest open source implementation of a learning management system with over 160,000 users</li>\r\n<li style=\"box-sizing: border-box;\">Managed team of eight developers worldwide while acting as Release Manager numerous large work packages for git integration</li>\r\n<li style=\"box-sizing: border-box;\">Produced an automated nightly sync to import full PeopleSoft data into the totara system that completes in less than 20 minutes versus 4 days for the original contributed module</li>\r\n</ul>','2011-08-01','2012-03-01','2017-11-21 05:39:41','2017-11-21 05:39:41'),(8,'Myriad Interactive','Phoenix, AZ','https://www.myriadinteractive.com/','jobs/November2017/gRwnVfibS7XozbLunsc2.png','PHP Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Implemented complex dynamic Drupal sites for ASU and others (asugammage.com, nacts.asu.edu, herbergeracademy.asu.edu, livewellcolorado.com, library.arizona.edu)</li>\r\n<li style=\"box-sizing: border-box;\">Developed many large corporate sites using Zend (swlaw.com, onlinece.speareducation.com, outreach.colorado.edu, eatwellbewell.org, maracayhomes.com)</li>\r\n<li style=\"box-sizing: border-box;\">Integrated OpenCart, Magento, and Wordpress into many existing sites.</li>\r\n</ul>','2008-11-01','2011-08-01','2017-11-21 05:41:05','2017-11-21 05:41:05'),(9,'isocurve inc.','New York, NY','http://www.google.com/search?q=isocurve','jobs/November2017/JYBPQwySp6uN1iRyIoS1.gif','PHP Developer','<ul class=\"responsibilities\">\r\n<li style=\"box-sizing: border-box;\">Utilized CakePHP framework to develop an internal project management system and miscellaneous small site applications</li>\r\n<li style=\"box-sizing: border-box;\">Managed multiple shared hosting accounts including e-mails, configurations, and databases</li>\r\n<li style=\"box-sizing: border-box;\">Designed data schemas for small to large projects including a data solution for GE Real Estate multi-national website.</li>\r\n</ul>','2006-10-01','2008-03-01','2017-11-21 05:44:00','2017-11-21 05:45:57'),(10,'Multiple','Multiple','http://aaron.ganschow.us/','jobs/November2017/IpH5jooiGrWSDaLS7oNU.gif','Contracted PHP Developer','<p>&nbsp; &nbsp;&nbsp;</p>','1998-06-01','2011-08-01','2017-11-21 05:48:35','2017-11-21 05:48:35');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.media.index',NULL),(3,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.posts.index',NULL),(4,1,'Users','','_self','voyager-person',NULL,NULL,3,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.users.index',NULL),(5,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.categories.index',NULL),(6,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.pages.index',NULL),(7,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.roles.index',NULL),(8,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL,NULL),(9,1,'Menu Builder','','_self','voyager-list',NULL,8,10,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.menus.index',NULL),(10,1,'Database','','_self','voyager-data',NULL,8,11,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.database.index',NULL),(11,1,'Compass','/admin/compass','_self','voyager-compass',NULL,8,12,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL,NULL),(12,1,'Hooks','/admin/hooks','_self','voyager-hook',NULL,8,13,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL,NULL),(13,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2017-11-20 21:28:11','2017-11-20 21:28:11','voyager.settings.index',NULL),(14,1,'Skill Categories','/admin/skill-categories','_self',NULL,NULL,NULL,15,'2017-11-20 22:02:09','2017-11-20 22:02:09',NULL,NULL),(15,1,'Skills','/admin/skills','_self',NULL,NULL,NULL,16,'2017-11-20 22:02:56','2017-11-20 22:02:56',NULL,NULL),(16,1,'Jobs','/admin/jobs','_self',NULL,NULL,NULL,17,'2017-11-20 22:05:28','2017-11-20 22:05:28',NULL,NULL),(17,1,'Portfolio Pieces','/admin/portfolio-pieces','_self',NULL,NULL,NULL,18,'2017-11-20 22:08:06','2017-11-20 22:08:06',NULL,NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2017-11-20 21:28:11','2017-11-20 21:28:11');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (119,'2014_10_12_000000_create_users_table',1),(120,'2014_10_12_100000_create_password_resets_table',1),(121,'2016_01_01_000000_add_voyager_user_fields',1),(122,'2016_01_01_000000_create_data_types_table',1),(123,'2016_01_01_000000_create_pages_table',1),(124,'2016_01_01_000000_create_posts_table',1),(125,'2016_02_15_204651_create_categories_table',1),(126,'2016_05_19_173453_create_menu_table',1),(127,'2016_10_21_190000_create_roles_table',1),(128,'2016_10_21_190000_create_settings_table',1),(129,'2016_11_30_135954_create_permission_table',1),(130,'2016_11_30_141208_create_permission_role_table',1),(131,'2016_12_26_201236_data_types__add__server_side',1),(132,'2017_01_13_000000_add_route_to_menu_items_table',1),(133,'2017_01_14_005015_create_translations_table',1),(134,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(135,'2017_01_15_000000_create_permission_groups_table',1),(136,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(137,'2017_03_06_000000_add_controller_to_data_types_table',1),(138,'2017_04_11_000000_alter_post_nullable_fields_table',1),(139,'2017_04_21_000000_add_order_to_data_rows_table',1),(140,'2017_07_05_210000_add_policyname_to_data_types_table',1),(141,'2017_08_05_000000_add_group_to_settings_table',1),(142,'2017_11_08_002141_create_skill_categories_table',1),(143,'2017_11_08_002304_create_skills_table',1),(144,'2017_11_08_002704_create_jobs_table',1),(145,'2017_11_08_002755_create_portfolio_pieces_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(2,'browse_database',NULL,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(3,'browse_media',NULL,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(4,'browse_compass',NULL,'2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(5,'browse_menus','menus','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(6,'read_menus','menus','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(7,'edit_menus','menus','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(8,'add_menus','menus','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(9,'delete_menus','menus','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(10,'browse_pages','pages','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(11,'read_pages','pages','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(12,'edit_pages','pages','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(13,'add_pages','pages','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(14,'delete_pages','pages','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(15,'browse_roles','roles','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(16,'read_roles','roles','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(17,'edit_roles','roles','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(18,'add_roles','roles','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(19,'delete_roles','roles','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(20,'browse_users','users','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(21,'read_users','users','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(22,'edit_users','users','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(23,'add_users','users','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(24,'delete_users','users','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(25,'browse_posts','posts','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(26,'read_posts','posts','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(27,'edit_posts','posts','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(28,'add_posts','posts','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(29,'delete_posts','posts','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(30,'browse_categories','categories','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(31,'read_categories','categories','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(32,'edit_categories','categories','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(33,'add_categories','categories','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(34,'delete_categories','categories','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(35,'browse_settings','settings','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(36,'read_settings','settings','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(37,'edit_settings','settings','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(38,'add_settings','settings','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(39,'delete_settings','settings','2017-11-20 21:28:11','2017-11-20 21:28:11',NULL),(40,'browse_skill_categories','skill_categories','2017-11-20 22:02:09','2017-11-20 22:02:09',NULL),(41,'read_skill_categories','skill_categories','2017-11-20 22:02:09','2017-11-20 22:02:09',NULL),(42,'edit_skill_categories','skill_categories','2017-11-20 22:02:09','2017-11-20 22:02:09',NULL),(43,'add_skill_categories','skill_categories','2017-11-20 22:02:09','2017-11-20 22:02:09',NULL),(44,'delete_skill_categories','skill_categories','2017-11-20 22:02:09','2017-11-20 22:02:09',NULL),(45,'browse_skills','skills','2017-11-20 22:02:56','2017-11-20 22:02:56',NULL),(46,'read_skills','skills','2017-11-20 22:02:56','2017-11-20 22:02:56',NULL),(47,'edit_skills','skills','2017-11-20 22:02:56','2017-11-20 22:02:56',NULL),(48,'add_skills','skills','2017-11-20 22:02:56','2017-11-20 22:02:56',NULL),(49,'delete_skills','skills','2017-11-20 22:02:56','2017-11-20 22:02:56',NULL),(50,'browse_jobs','jobs','2017-11-20 22:05:28','2017-11-20 22:05:28',NULL),(51,'read_jobs','jobs','2017-11-20 22:05:28','2017-11-20 22:05:28',NULL),(52,'edit_jobs','jobs','2017-11-20 22:05:28','2017-11-20 22:05:28',NULL),(53,'add_jobs','jobs','2017-11-20 22:05:28','2017-11-20 22:05:28',NULL),(54,'delete_jobs','jobs','2017-11-20 22:05:28','2017-11-20 22:05:28',NULL),(55,'browse_portfolio_pieces','portfolio_pieces','2017-11-20 22:08:06','2017-11-20 22:08:06',NULL),(56,'read_portfolio_pieces','portfolio_pieces','2017-11-20 22:08:06','2017-11-20 22:08:06',NULL),(57,'edit_portfolio_pieces','portfolio_pieces','2017-11-20 22:08:06','2017-11-20 22:08:06',NULL),(58,'add_portfolio_pieces','portfolio_pieces','2017-11-20 22:08:06','2017-11-20 22:08:06',NULL),(59,'delete_portfolio_pieces','portfolio_pieces','2017-11-20 22:08:06','2017-11-20 22:08:06',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_pieces`
--

DROP TABLE IF EXISTS `portfolio_pieces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_pieces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_pieces`
--

LOCK TABLES `portfolio_pieces` WRITE;
/*!40000 ALTER TABLE `portfolio_pieces` DISABLE KEYS */;
INSERT INTO `portfolio_pieces` VALUES (1,'Ingestion Digest','<p>September 1st, 2009 I decided to begin photographing every meal I ate. I passionately photographed every calorie I put in my body until September 2010.&nbsp;<a href=\"http://clintforrester.com/\" target=\"_blank\" rel=\"noopener\">Clint Forrester</a> provided the Illustration and I turned his composition into HTML, CSS and ultimately a Tumblr theme.</p>','http://www.ingestiondigest.com/','portfolio-pieces/November2017/R1gbb8Uj8orpIlHQwygg.png','2017-11-21 06:00:04','2017-11-21 06:00:04'),(2,'ASU Herberger Academy','<p>fasd&nbsp;</p>','https://herbergeracademy.asu.edu/','portfolio-pieces/November2017/Z2wvV0q6BrjGc7FVgS3v.jpg','2017-11-21 06:01:18','2017-11-21 06:01:18'),(3,'Catholic Charities Donations','<p>Contracted to create a more streamlined donation page. Existing donation page was externally hosted by Blackbaud. Refined from four to one pages. Allowance for multiple campaigns and alterable campaigns. Integration with TransFirst SOAP SSL transactions</p>','https://www.catholiccharitiesaz.org/donate-now','portfolio-pieces/November2017/YO21HuUhu8D3be1SaBPO.png','2017-11-21 06:02:47','2017-11-21 06:02:47'),(4,'UofA Libraries','<p>A Drupal Theme implemented for UA Libraries while working for Myriad Interactive. HTML, CSS and jQuery used to implement features from Photoshop composition provided by Esser Design. Drupal theme and site structure implemented then provided to the development team at The University of Arizona</p>','http://new.library.arizona.edu/','portfolio-pieces/November2017/NilPBSPZfYkVbyCVvaL7.png','2017-11-21 06:03:45','2017-11-21 06:03:45'),(6,'Live Well Colorado','<p>Another Drupal powered site implemented for Myriad Interactive. HTML and CSS created from Photoshop composition and then turned into a Drupal Theme. CCK, Views, and a few other modules bring this site together. Content and site maintenence are in full control of LiveWell Colorado.</p>','NA','portfolio-pieces/November2017/R6MtzHeGcb9fWVNVCpk9.png','2017-11-21 17:55:15','2017-11-21 17:55:15'),(7,'E.P. Carillo','<p>Subcontract for Click3Xto develop an HTML template with CSS and Javascript to handle dynamic elements of the site. Later implementation of Google Maps and Twitter were done by another subcontractor.</p>','http://epcarrillo.com/','portfolio-pieces/November2017/BZYMv2d2zVAYtrp7ZCA1.png','2017-11-21 17:58:19','2017-11-21 17:58:19'),(8,'Ford Urban','<p>Subcontract for Click3X to implement HTML and CSS for the Ford Urban site. jQuery used for Flash inclusion and backwards comparability of drop-down menus. Final implementation and Flash created by Click3X.</p>','NA','portfolio-pieces/November2017/3vTf4hHvxskXxkSQtSwI.png','2017-11-21 18:00:17','2017-11-21 18:00:17'),(9,'Mustard Seed School','<p>NA</p>','http://www.mustardseedschool.org/','portfolio-pieces/November2017/K6d2fVxddSbEQQYcMRe1.jpg','2017-11-21 18:02:15','2017-11-21 18:02:15'),(10,'Transpack','<p>I was approached by my previous VP of Production from isocurve to help complete this small project. Their previous site was a bit dated and difficult to navigate. They wanted a fresher, cleaner look, and came to Ephraim Kehlmann for the new site.</p>','NA','portfolio-pieces/November2017/ISuQxiqlbIpdvjyP5Jos.jpg','2017-11-21 18:04:40','2017-11-21 18:04:40'),(11,'AKC Gallery','<p>This was my final major project for isocurve in March of 2008. The owner of isocurve, Rachel Allgood, teamed up with Mitch Meisner to gain rights to the vast collection of paintings that the AKC owns. They gained the sole rights to reproduce these famous paintings in this way.</p>','NA','portfolio-pieces/November2017/sFNwvrdOm7fcJniSM3QY.jpg','2017-11-21 18:06:00','2017-11-21 18:07:06'),(12,'Gabriela De La Vega Store','<p>This e-commerce section of gabrieladelavega.com was built by isocurve to retain the feeling of the flash site, while being HTML driven and as light on load as possible. The design was created by Jason Robinson and I turned the main PSD into HTML and a theme for Squirrel Cart.</p>','NA','portfolio-pieces/November2017/48UI7jwvmlfCYBJxnAfF.jpg','2017-11-21 18:09:09','2017-11-21 18:09:09');
/*!40000 ALTER TABLE `portfolio_pieces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_pieces_skills`
--

DROP TABLE IF EXISTS `portfolio_pieces_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio_pieces_skills` (
  `portfolio_piece_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_pieces_skills`
--

LOCK TABLES `portfolio_pieces_skills` WRITE;
/*!40000 ALTER TABLE `portfolio_pieces_skills` DISABLE KEYS */;
INSERT INTO `portfolio_pieces_skills` VALUES (1,2),(1,4),(1,7),(1,31),(2,1),(2,2),(2,3),(2,4),(2,7),(2,10),(3,2),(3,4),(3,7),(3,19),(3,20),(3,34),(4,1),(4,2),(4,3),(4,4),(4,7),(4,10),(5,1),(5,2),(5,3),(5,4),(5,7),(5,10),(6,1),(6,2),(6,3),(6,4),(6,7),(6,10),(7,2),(7,4),(7,7),(8,2),(8,4),(8,7),(9,1),(9,2),(9,3),(9,4),(9,7),(9,10),(10,1),(10,2),(10,3),(10,4),(10,7),(11,2),(11,4),(11,7),(11,17),(11,31),(12,2),(12,4),(12,7),(12,31);
/*!40000 ALTER TABLE `portfolio_pieces_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `related_skills`
--

DROP TABLE IF EXISTS `related_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `related_skills` (
  `skill_1_id` int(11) NOT NULL,
  `skill_2_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `related_skills`
--

LOCK TABLES `related_skills` WRITE;
/*!40000 ALTER TABLE `related_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `related_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2017-11-20 21:28:11','2017-11-20 21:28:11'),(2,'user','Normal User','2017-11-20 21:28:11','2017-11-20 21:28:11');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_categories`
--

DROP TABLE IF EXISTS `skill_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_categories`
--

LOCK TABLES `skill_categories` WRITE;
/*!40000 ALTER TABLE `skill_categories` DISABLE KEYS */;
INSERT INTO `skill_categories` VALUES (1,'Languages',1,'2017-11-20 22:09:07','2017-11-20 22:09:07'),(2,'Front End Frameworks',2,'2017-11-20 22:09:00','2017-11-20 22:10:52'),(3,'Back End Frameworks',3,'2017-11-20 22:09:31','2017-11-20 22:09:31'),(4,'CMS / E-Commerce Platforms',4,'2017-11-20 22:10:23','2017-11-20 22:10:23'),(5,'Other Skills',5,'2017-11-20 22:10:40','2017-11-20 22:10:40');
/*!40000 ALTER TABLE `skill_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skill_category_id` int(11) NOT NULL,
  `skill_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,1,'skills/November2017/UcpBAwkOSvvubRDemUw5.png','PHP','<p>description of PHP</p>',100,'2000-05-01',1,'2017-11-20 22:18:00','2017-11-20 22:21:47'),(2,1,'skills/November2017/x7BA7e0ShjO8GWkTGqoi.png','HTML 5 / CSS 3','<p>Description</p>',100,'1997-01-01',2,'2017-11-20 22:26:40','2017-11-20 22:26:40'),(3,1,'skills/November2017/0aulEPyt5WEUGBkU6Bns.png','MySQL','<p>desc</p>',95,'2000-07-01',3,'2017-11-20 23:06:06','2017-11-20 23:06:06'),(4,1,'skills/November2017/vxvx2cBTboFwRByU3NKZ.png','JavaScript','<p>JS</p>',85,'1998-08-01',4,'2017-11-20 23:06:00','2017-11-21 05:35:25'),(5,1,'skills/November2017/z335OLDQjBsA95XxjE2K.png','Sass','<p>Sass: Syntactically Awesome Stylesheets</p>',85,'2010-07-01',5,'2017-11-20 23:07:00','2017-11-21 01:41:16'),(6,2,'skills/November2017/ePtAoyZycD78djM27EzL.png','Twitter Bootstrap','<p>bootstrap 4</p>',95,'2011-01-01',6,'2017-11-20 23:09:06','2017-11-20 23:09:06'),(7,2,'skills/November2017/XGXimjB8bWlQYBXTBodX.png','JQuery','<p>Jquery</p>',90,'2002-01-01',7,'2017-11-20 23:09:46','2017-11-20 23:09:46'),(8,3,'skills/November2017/3LdqNVPEceQOefixJCoX.png','Laravel','<p>laravel</p>',80,'2017-10-01',9,'2017-11-20 23:11:51','2017-11-20 23:11:51'),(9,3,'skills/November2017/eLVnAYAarjlUKVOSqUpo.png','Symfony','<p>gsdf fgd</p>',80,'2007-05-01',10,'2017-11-20 23:12:33','2017-11-20 23:12:33'),(10,4,'skills/November2017/waFlw9NOzUePCMY9R64v.png','Drupal','<p>drupal</p>',100,'2003-01-01',11,'2017-11-20 23:13:36','2017-11-20 23:13:36'),(11,4,'skills/November2017/6F73auygZMit3qhZMLAB.png','Wordpress','<p>wp</p>',95,'2000-03-01',12,'2017-11-20 23:14:28','2017-11-20 23:14:28'),(12,4,'skills/November2017/Ed7wM4rYwg7lh4ZcvnY7.png','Magento','<p>mah</p>',80,'2014-05-01',13,'2017-11-20 23:15:17','2017-11-20 23:15:17'),(13,4,'skills/November2017/l8qeAXLJ1mrsOV2aobp4.png','Bigcommerce','<p>FE Bigcommerce</p>',85,'2017-05-01',14,'2017-11-20 23:16:28','2017-11-20 23:16:28'),(14,5,'skills/November2017/K2VSwFaSnggYZJUNDZzW.png','git','<p>git</p>',100,'2009-08-01',16,'2017-11-20 23:17:10','2017-11-20 23:17:10'),(15,5,'skills/November2017/hI886wAO92QYBOgeLZzf.png','JIRA','<p>Atlassian</p>',90,'2000-01-01',20,'2017-11-20 23:47:00','2017-11-21 02:04:27'),(16,1,'skills/November2017/1bbLSgcFBVHwWlGqCOtY.png','JSON','<p>JSON</p>',100,'2003-06-01',20,'2017-11-21 00:14:41','2017-11-21 00:14:41'),(17,1,'skills/November2017/i4LbZMMihuunrVxUkXhP.png','Perl','<p>perl</p>',65,'1998-01-01',50,'2017-11-21 00:15:26','2017-11-21 00:15:26'),(18,1,'skills/November2017/xtwYKVWvVzQNF5zMfy6r.png','PostgreSQL','<p>pgsql</p>',68,'2013-05-01',49,'2017-11-21 00:16:16','2017-11-21 00:16:16'),(19,1,'skills/November2017/CiIcxlBAkejsyRwT0WA7.png','C#','<p>csharp</p>',50,'2009-01-01',70,'2017-11-21 00:16:54','2017-11-21 00:16:54'),(20,1,'skills/November2017/mRUkZPD8rGkhtxKbzQto.png','Microsoft SQL Server','<p>sql</p>',75,'2000-01-01',71,'2017-11-21 00:17:39','2017-11-21 00:17:39'),(21,5,'skills/November2017/juGIo4vYMcJK08KU7Bpm.png','Apache','<p>apache</p>',95,'1998-01-01',25,'2017-11-21 00:20:55','2017-11-21 00:20:55'),(22,3,'skills/November2017/2X55lhdNGWoe1ooYydnM.png','CakePHP','<p>cakephp</p>',75,'2007-01-01',26,'2017-11-21 00:21:32','2017-11-21 00:21:32'),(23,5,'skills/November2017/Cl4JsUX7a1gWYLrffpzj.png','Composer','<p>composer</p>',85,'2012-01-01',27,'2017-11-21 00:22:03','2017-11-21 00:22:03'),(24,2,'skills/November2017/z2Djwh1yjNzwtwl9IrTT.png','handlebars','<p>handlebars</p>',85,'2017-05-05',28,'2017-11-21 00:22:41','2017-11-21 00:22:41'),(25,1,'skills/November2017/v8QSU6DydLYdeIYPz5PJ.png','Java','<p>java</p>',60,'2004-08-01',29,'2017-11-21 00:23:30','2017-11-21 00:23:30'),(26,5,'skills/November2017/lfJxVEcvt29HHa8lRe7K.png','Linux','<p>linux</p>',90,'1998-01-01',35,'2017-11-21 00:25:24','2017-11-21 00:25:24'),(27,5,'skills/November2017/cncEmEcX1Hz2rh9VWSiy.png','memcached','<p>memcached</p>',57,'2013-05-01',55,'2017-11-21 00:31:20','2017-11-21 00:31:20'),(28,4,'skills/November2017/YDXyJnsjJmd06ltNkL6Q.png','moodle','<p>moodle</p>',55,'2012-08-01',56,'2017-11-21 00:35:11','2017-11-21 00:35:11'),(29,5,'skills/November2017/uMkUTeGbtcVDGWDYP8pf.png','nginx','<p>nginx</p>',80,'2014-11-01',57,'2017-11-21 00:36:25','2017-11-21 00:36:25'),(30,5,'skills/November2017/VOUlLV3Azv2zNaoQFwt3.png','nodejs','<p>nodejs</p>',70,'2012-05-01',44,'2017-11-21 00:37:16','2017-11-21 00:37:16'),(31,5,'skills/November2017/Xy8seyxksRJvHupaPsY3.png','Adobe Photoshop','<p>photoshop</p>',85,'1999-01-01',54,'2017-11-21 00:38:07','2017-11-21 00:38:07'),(32,4,'skills/November2017/1RAloe7PWRNrHidBTvRc.png','Prestashop','<p>prestashop</p>',80,'2016-09-15',30,'2017-11-21 00:39:00','2017-11-21 01:43:24'),(33,5,'skills/November2017/d1iaj6T8OOvuS5gDXRTc.jpg','Responsive design','<p>responsive design</p>',100,'2007-09-01',88,'2017-11-21 00:40:35','2017-11-21 00:40:35'),(34,5,'skills/November2017/JSkmzuT31iSDQ29jkKEM.png','REST API','<p>rest</p>',95,'2000-01-01',66,'2017-11-21 00:41:18','2017-11-21 00:41:18'),(35,5,'skills/November2017/aFbvvonB0ZGx9y5SnkPW.png','Search Engine Optimization','<p>seo</p>',80,'1999-03-01',24,'2017-11-21 00:41:56','2017-11-21 00:41:56'),(36,4,'skills/November2017/vJKWSGfgrSQo3j5BrbCa.png','Squirrelcart','<p>sqcart</p>',50,'2007-03-01',68,'2017-11-21 00:42:51','2017-11-21 00:42:51'),(37,4,'skills/November2017/sNCAFGukySwVtm6yhIke.png','totara lms','<p>totara</p>',55,'2011-08-01',65,'2017-11-21 00:43:00','2017-11-21 01:42:35'),(38,2,'skills/November2017/pOrryNZ0dEqNEmQA4Fca.png','twig','<p>twig</p>',100,'2014-05-01',81,'2017-11-21 00:44:45','2017-11-21 00:44:45'),(39,5,'skills/November2017/COa8P5FBx9Ejwe0EI7Dw.png','Vagrant','<p>Vagrant up!</p>',85,'2013-06-01',2,'2017-11-21 00:45:24','2017-11-21 00:45:24'),(40,3,'skills/November2017/qwlLJI17UuNhY3VhJm86.png','Zend Framework','<p>zend</p>',80,'2008-11-01',56,'2017-11-21 00:46:03','2017-11-21 00:46:03');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Aaron Ganschow','wildleaf@gmail.com','users/default.png','$2y$10$h3Yl51TKRSmYppwpdUj3N.oxLMij01xMDImtIH.rcwMRqJu8XIdty',NULL,'2017-11-20 21:28:36','2017-11-20 21:28:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-22 21:27:50
